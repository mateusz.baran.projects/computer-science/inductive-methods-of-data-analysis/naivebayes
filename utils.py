from collections import Iterable
from itertools import product

import numpy as np
from sklearn.metrics import accuracy_score, confusion_matrix, f1_score, precision_score, recall_score
from sklearn.model_selection import cross_validate
from sklearn.preprocessing import KBinsDiscretizer
from sklearn.preprocessing import OneHotEncoder


def print_score(y_true, y_pred):
    print('Acc:          ', accuracy_score(y_true, y_pred))
    print('f1 avg:       ', f1_score(y_true, y_pred, average='macro', zero_division=0))
    print('Recall avg:   ', recall_score(y_true, y_pred, average='macro', zero_division=0))
    print('Precision avg:', precision_score(y_true, y_pred, average='macro', zero_division=0))
    print('Recall:       ', recall_score(y_true, y_pred, average=None, zero_division=0))
    print('Precision:    ', precision_score(y_true, y_pred, average=None, zero_division=0))
    print('f1:           ', f1_score(y_true, y_pred, average=None, zero_division=0))
    print('Confusion matrix:')
    print(confusion_matrix(y_true, y_pred))


def discretize_dataset(dataset, columns, n_bins=10, encode='ordinal', strategy='uniform', inplace=False):
    if not inplace:
        dataset = dataset.copy()

    if isinstance(n_bins, Iterable):
        for c, b, s in zip(columns, n_bins, strategy):
            discretizer = KBinsDiscretizer(n_bins=b, encode='ordinal', strategy=s)
            dataset[c] = discretizer.fit_transform(dataset[[c]])
    else:
        discretizer = KBinsDiscretizer(n_bins=n_bins, encode=encode, strategy=strategy)
        dataset[columns] = discretizer.fit_transform(dataset[columns])

    dataset.drop_duplicates(inplace=True)

    return dataset


def evaluation(dataset, d_cols, x_cols, y_col, model, cv, bins, strategies,
               default_bins, default_strategy, scoring='f1_macro'):
    eval_dict = {
        'feature_idx': list(range(len(d_cols))),
        'n_bins': bins,  # [5, 8, 11]
        'strategy': strategies,  # ['uniform', 'quantile', 'kmeans']
    }

    results = []

    for feature_idx, bins, strategy in product(*eval_dict.values()):
        d_cols_ = d_cols[:]
        d_col_ = d_cols_.pop(feature_idx)
        df = discretize_dataset(dataset, [d_col_], n_bins=bins, strategy=strategy)
        if d_cols_:
            discretize_dataset(df, d_cols_, n_bins=default_bins, strategy=default_strategy, inplace=True)

        X = OneHotEncoder().fit_transform(df[x_cols])
        y = df[y_col]

        cv_result = cross_validate(model, X, y, cv=cv, scoring=scoring)
        results.append((feature_idx, bins, strategy, cv_result['test_score'].mean()))

    return results


def evaluate_cross_validation(model, X, y, cv, k_splits):
    results = []
    errors = []

    for k_split in k_splits:
        mean = []
        std = []
        for i in range(50):
            sk_fold = cv(n_splits=k_split, random_state=i, shuffle=True)
            cv_dict = cross_validate(model, X, y, cv=sk_fold, scoring='f1_macro')
            mean.append(cv_dict['test_score'].mean())
            std.append(cv_dict['test_score'].std())

        results.append(np.mean(mean))
        errors.append(np.mean(std))

    return results, errors


def evaluation_all_features(dataset, d_cols, x_cols, y_col, model,
                            cv, n_bins, strategy, scoring='f1_macro'):
    eval_dict = {
        'n_bins': n_bins,
        'strategy': strategy,
    }

    results = []

    for n_bins, strategy in product(*eval_dict.values()):
        df = discretize_dataset(dataset, d_cols, n_bins=n_bins, strategy=strategy)

        X = OneHotEncoder().fit_transform(df[x_cols])
        y = df[y_col]

        cv_result = cross_validate(model, X, y, cv=cv, scoring=scoring)
        results.append((n_bins, strategy, cv_result['test_score'].mean()))

    return results
